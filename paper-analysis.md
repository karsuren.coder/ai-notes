# Reference

- [Paper - google drive - open with Adobe for comments](https://drive.google.com/file/d/1E11Fme95gM_0yV7PjVh9-Uj7LtzlYthY/view?usp=share_link)

# Core Idea - what this is paper about

Given: A completion engine based on a language model GPT-3 (not gpt-3.5).

Observation: By itself the completion engine is bad at reasoning problems.

Experiment: Give a few example reasonings as part of the completion input, so that it will continue generating outputs in that style.

Take away: with a large enough model (100B + params): this works -- indicating at that size, it has figured out the pattern of coherent chain of thought

# Discussion

## Excerpt

> shows that chain-of-thought prompting is an emergent ability of model scale (Wei et al., 2022b). That is, chain-of-thought prompting does not positively impact performance for small models, and only yields performance gains when used with models of ∼100B parameters.

### Argument 1

- Karthik: This is quite an interesting result because, all this is saying is keep using the 'simple' training model, but scale it out (in terms of no of learning parameters of the model and the data set) and that is all you need to do, and it will figure out even the pattern of reasoning (as an emergent phenonmenon) -- you don't need to do supervised training (tell it, hey this is not a valid argument, OR this is a valid argument). Just how capable is this??!! What is the limit of this training model, what happens if you have 100 trillion params!!
- Jibin: Are you surprised by the fact that the pattern of reasoning is embedded within language
- Karthik: I guess I shouldn't be. Maybe surprising is not the right word. Surprising is the first feeling. But even after coming to terms with it, it is still incredible.

### Argument 2

- Jibin: If the model itself has figured out the pattern to create coherent arguments, then why does it not already do that?
- Bitto & Karthik: At it's core this is about making GPT-3 (not GPT-3.5) which is a completion engine do completions in a manner that uses coherent reasoning. For a prompt like `"The cafeteria had 23 apples. If they used 20 to make lunch and bought 6 more, how many apples do they have?"` there are many possible completion patterns such as directly giving the answer without reasoning. The pattern infered from **`Question-Answer`** (`"The answer is 27."`) sort of data points does not have enough information to get to the correct answer (or maybe that would take a lot more training data to figure out). But **`Question-Argument-Answer`** (`"The cafeteria had 23 apples originally. They used 20 to make lunch. So they had 23 - 20 = 3. They bought 6 more apples, so they have 3 + 6 = 9. The answer is 9."`) kind of data sets have more patterns for it to figure out the reasoning via training. So, providing the similar prompts tells it to follow the pathway which has the pattern for argument.

# Discussion

## Excerpt

> scaling up model size alone has not proved sufficient for achieving high performance on challenging tasks such as arithmetic, commonsense, and symbolic reasoning. [..] **Prior work has given models the ability to generate natural language intermediate steps by training from scratch (Ling et al., 2017) or finetuning a pretrained model**

## Additional Context

When we gave the same input that requires a coherent reasoning to chat-GPT (gpt-3.5), it was already giving a coherent reasoning, and a correct answer.

## Discussion

- Bitto: Maybe they did something like this for chat-GPT : **Prior work has given models the ability to generate natural language intermediate steps by training from scratch (Ling et al., 2017) or finetuning a pretrained model**
- Karthik: Also, chat-GPT is not just a simple completion engine like GPT-3. It has additional `Reinforcement Learning from Human Feedback (RLHF)` (this corresponing to the 2nd proposal -- **... finetuning a pretrained model**) - we don't know what that involves. At least not yet - there are a couple of paper on this - Instruct GPT (method used by chat-GPT -- quite expensive) and Self Instruct (a more efficient technique which assumes something like chat-GPT already exists -- very very efficient) -- these techniques are used to convert a completion engine like GPT-3 into chat GPT. Without understanding these techniques, it is not productive to hypothesise the reason why chat-GPT has a certain ability.

## Discussion

# Key takeaways

1. Large LLM (generated from transformer training paradigm alone) -- will start figuring out the pattern for even 'coherent reasoning' when they are big enough (~100B).
2. Doesn't matter what fancy prompting you are using if it doesn't have lot of params it won't magically learn reasoning because of your prompt.
> - Infact if the no of params is really low, results will be worse.
3. The 'prompting' part of the 'Chain of the thought prompting' is just smart prompt engineering -- kind of like giving good google search prompt (by google search I mean the version of google search in 2022)

# Discussion

## Context

> The goal of this paper is to endow language models with the ability to generate a similar chain of thought

## Discussion

- Karthik: This is misleading. This is not endowing the model with anything. The model already has it. All you are doing is using a prompting style which better narrows down to the model the relavant pattern it has already figured out to use.

# Later reading

- https://huggingface.co/blog/rlhf

# Pending reading on this doc

- Symbolic reasoning -- section 5
- FAQ section

# Next Step

- Use the library: check documentation to see if there is verbose log flag or something and see if we can know how langChain is using chat-GPT.
- If needed, read the source to find out that.
- Let's see insights come to us after learning how they use chat-GPT, considering we already have some insights on how chain of thought works. Who knows maybe we will have a better idea on how to use it.